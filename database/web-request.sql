/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 100128
Source Host           : localhost:3306
Source Database       : web-request

Target Server Type    : MYSQL
Target Server Version : 100128
File Encoding         : 65001

Date: 2018-07-14 16:20:20
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tb_request
-- ----------------------------
DROP TABLE IF EXISTS `tb_request`;
CREATE TABLE `tb_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unitName` varchar(255) DEFAULT NULL,
  `projectName` varchar(255) DEFAULT NULL,
  `persons` varchar(255) DEFAULT NULL,
  `projectDetails` varchar(255) DEFAULT NULL,
  `projectNeeds` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_request
-- ----------------------------

-- ----------------------------
-- Table structure for tb_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_user
-- ----------------------------
INSERT INTO `tb_user` VALUES ('1', 'admin', 'admin', 'admin');
INSERT INTO `tb_user` VALUES ('2', 'dev', 'admin', null);
