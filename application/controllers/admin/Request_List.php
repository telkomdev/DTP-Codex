<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Request_List extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
    }

	function index() {
		$sessionName = $this->session->logged_in;
		if ( $sessionName && $sessionName['username'] && $sessionName['role'] ) {
			if ( $sessionName['role'] != 'admin' ) {
				redirect('form/request');
			}
		}else{
			redirect('login');
		}
		$data = array(
					'data'		=> $this->request_model->get_all(),
					'flashdata' => $this->session->flashdata(),
				);
		$this->layout_dashboard->show_layout("request", "index", $data);
		// $this->load->view('request/index', $data);
	}
}