<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Login extends CI_Controller {

	function index() {
		if ( $this->session->logged_in ) {
			redirect('admin/request_list');
		}
		$this->load->helper(array('form'));
		$this->load->view('login/index');
	}

	function verify_login() {
		//This method will have the credentials validation
		$this->load->library('form_validation');

		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');

		if($this->form_validation->run() == FALSE){ $this->load->view('login/index'); }
		else {
			$username 	= $this->input->post('username');
			$password 	= $this->input->post('password');
			$query 		= $this->user_model->get_user( $username, $password );

			if ( count($query) == 1 ) {
				$sess_array = array(
	         		'username'	=> $query[0]->username,
	         		'role'		=> $query[0]->role,
	       		);
		       	$this->session->set_userdata('logged_in', $sess_array);

				if($this->session->userdata('logged_in')) {
					redirect('admin/request_list');
				}
				else { redirect('login'); }	
			}
		}
		redirect('Login', 'refresh');
	}

	function logout() {
		$this->session->unset_userdata('logged_in');
		redirect('login');
	}

}