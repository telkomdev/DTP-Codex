<div class="panel panel-default" id="thanks_alot">
	<div class="panel-heading">
    	<h3 class="panel-title">List Request</h3>
    </div>
    <div class="panel-body">
    	<div class="table-responsive">
			<table class="table table-bordered table-striped datatables">
				<thead>
					<tr>
						<th>No</th>
						<th>Unit Name</th>
						<th>Persons</th>
						<th>Project Name</th>
						<th>Project Needs</th>
						<th>Project Detail</th>
					</tr>
				</thead>
				<tbody>
					<?php //print_r ($offset);die;
						$no = 0;
						// Memastikan jika data tidak kosong
						if ($data->num_rows() > 0) {
							foreach ($data->result() as $row) {
					?>
													
					<tr>
						<td><?php echo ++$no; ?></td>
						<td><?php echo $row->unitName			?></td>
						<td><?php echo $row->persons			?></td>
						<td><?php echo $row->projectName 		?></td>
						<td><?php echo $row->projectNeeds 		?></td>
						<td><?php echo $row->projectDetails		?></td>
					</tr>
											
					<?php
							}
						} 
						// jika data masih kosong, tampilkan pesan
						else {
							echo "<tr><td colspan='9' align='center'>Data tidak ditemukan !!</td></tr>";
						}
					?>
				</tbody>
			</table>
			<div id="content_check"></div>
			<br />
			<?php
				if (isset($this->pagination)) {
					echo $this->pagination->create_links(); 
				}else{}
			?>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$(".click").click(function(e) {
	       	e.preventDefault();
			var url = $(this).attr('href');
			if (url.indexOf('#') == 0) {
				$(url).modal('open');
			} else {
				$.get(url, function(data) { 
					$('.modal').modal('hide');
					$('<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">' + data + '</div>').modal();
				}).success(function() { $('input:text:visible:first').focus(); });
			}
	    });
	});
</script>