<!DOCTYPE html>
<html lang="en">
    <head>        
    	<?php $this->view('layouts/meta'); ?>                  
    </head>
    <body>
        <!-- START PAGE CONTAINER -->
        <div class="page-container">
            
            <!-- PAGE CONTENT -->
            <div class="page-content" style="margin-left: 0">
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12" style="margin-top: 50px">
                            
                            <form class="form-horizontal" name="request_form" action="<?php echo site_url('form/request/request') ?>" method="POST">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><strong>Form</strong> Request</h3>
                                    <ul class="panel-controls">
                                        <a href="<?php echo site_url('login/logout') ?>">Logout</a>
                                    </ul>
                                </div>
                                <div class="panel-body">
                                    <?php if( $flashdata ) { ?>                                                                        
                                    <div class="alert alert-<?php echo $flashdata['status']?>" role="alert">
                                        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                        <?php echo $flashdata['message']?>
                                    </div>
                                    <?php } ?>
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Unit Name</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control"/ name="unitName">
                                            </div>                                            
                                            <span class="help-block">field is required</span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Number of Persons</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control"/ name="persons">
                                            </div>                                            
                                            <span class="help-block">field is required</span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Project Name</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control"/ name="projectName">
                                            </div>                                            
                                            <span class="help-block">field is required</span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Project Needs</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control"/ name="projectNeeds">
                                            </div>                                            
                                            <span class="help-block">field is required</span>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Project Details</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <textarea class="form-control" rows="5" name="projectDetails"></textarea>
                                            <span class="help-block">field is required</span>
                                        </div>
                                    </div>

                                </div>
                                <div class="panel-footer">
                                    <button class="btn btn-default">Clear Form</button>                                    
                                    <input type="submit" name="submit_request" value="Submit" class="btn btn-primary pull-right" />
                                </div>
                            </div>
                            </form>
                            
                        </div>
                    </div>                    
                    
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                                                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->       
        
    <?php $this->view('layouts/js_default.php'); ?>              
    </body>
</html>






