<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <?php $this->view('layouts/meta'); ?>
    </head>
    <body>
        <div class="page-container">
            <?php $this->view('layouts/main_sidebar'); ?>

            <!-- PAGE CONTENT -->
            <div class="page-content">

                <?php
                    //<!-- HEADER -->
                    $this->view('layouts/header');
                    //<!-- END HEADER -->
                    echo '<ul class="breadcrumb"></ul>';
                ?>
                <div class="page-content-wrap">
                    <div class="row">
                        <div class="col-md-12">
                            <?php
                                echo $content;
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT -->
        </div>

        <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want to log out?</p>                    
                        <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <a href="<?php echo site_url('login/logout') ?>" class="btn btn-success btn-lg">Yes</a>
                            <button class="btn btn-default btn-lg mb-control-close">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->

        <!-- START PRELOADS -->
        <audio id="audio-alert" src="<?php echo base_url() ?>assets/audio/alert.mp3" preload="auto"></audio>
        <audio id="audio-fail" src="<?php echo base_url() ?>assets/audio/fail.mp3" preload="auto"></audio>
        <!-- END PRELOADS -->

        <?php
            $this->view('layouts/js_default');
        ?>
    </body>
</html>
