<div class="page-sidebar">
	<!-- START X-NAVIGATION -->
	<ul class="x-navigation">
		<li class="xn-logo">
			<a href="#">Admin</a>
			<a href="#" class="x-navigation-control"></a>
		</li>
		<li class="xn-title">Menu</li>

		<?php

			$menus = array(
					'admin/request_list'	=> '<span class="fa fa-desktop"></span><span class="xn-text">Dashboard</span>',
				);

			foreach ($menus as $link => $value) {
				$active = $link == $this->uri->segment(1) ? "class='active'" : "";
				echo "<li {$active}>", anchor($link,  $value ), "</li>";	
			}

		?>

	</ul>
<!-- END X-NAVIGATION -->
</div>